#####
# Quick intro to Codeowners
#
# The formmat is:
# [section]
# /<directory> <gitlab-user>
#
# The file is organized in to named sections denoting an internal organization function. e.g. People Group, Engineering, etc.
#
# ^[section] - Denotes an optional set of CodeOwners
#              No ^ means one of these Codeowners are required to approve MRs for this section
#
# For more information see https://docs.gitlab.com/ee/user/project/codeowners/
#
# Special notes:  Due to the current implementation in our code base sections can't have spaces in their names.
#                 Sections can't have named codeowners e.g. [section] @codeowner
#
#####

# These optional entries are for when no codeowner is found.
^[content-sites]
* @jamiemaynard @marshall007

^[job-families]
/content/job-families/ @sytses @streas

^[handbook]
/content/handbook/ @sytses @streas

^[company]
/content/company/ @sytses @streas

# The following entries relate to the operation of the handbook repo or the handbook site itself.
[handbook-operations]
/.gitignore @jamiemaynard @marshall007
/.gitlab-ci.yml @jamiemaynard @marshall007
/.markdownlint-cli2.jsonc @jamiemaynard @marshall007
/.markdownlint.yaml @jamiemaynard @marshall007
/go.sum @jamiemaynard @marshall007
/go.mod @jamiemaynard @marshall007
/package-lock.json @jamiemaynard @marshall007
/package.json @jamiemaynard @marshall007
/.gitlab/ @jamiemaynard @marshall007
/config/ @jamiemaynard @marshall007
/scripts/ @jamiemaynard @marshall007
/static/ @jamiemaynard @marshall007

[handbook-homepage]
/content/_index.html @jamiemaynard @marshall007
/content/featured-background.png @jamiemaynard @marshall007
/layouts/home.html @jamiemaynard @marshall007

[redirects]
/layouts/index.redirects @jamiemaynard @marshall007

[ide-configs]
/.editorconfig @jamiemaynard @marshall007
/.nova/ @jamiemaynard @marshall007
/.vscode/ @jamiemaynard @marshall007
/.idea/ @jamiemaynard @marshall007

[docs]
/LICENCE @jamiemaynard @marshall007
/README.md @jamiemaynard @marshall007
/content/docs/ @jamiemaynard @marshall007

# The rest of the entries are for the different sections of the handbook and/or job-families
[alliances]
/content/handbook/alliances/ @nbadiey
/content/job-families/alliances/ @nbadiey

[ceo]
/content/job-families/chief-executive-officer @sytses @streas
/content/job-families/board-of-directors/ @sytses @robin

[chief-information-security-officer]
/content/job-families/chief-information-security-officer.md @robin @fofungwu

[comms]
/content/news/ @sliang2

[customer-success]
/content/handbook/customer-success @spatching @christiaanconover @mrleutz

[engineering]
/content/handbook/engineering/horse/pubsec/ @sdumesnil @pmartinsgl @craig
/content/handbook/engineering/horse/ @marin
/content/handbook/engineering/infrastructure @sloyd @marin
/content/handbook/engineering/fedramp-compliance @connorgilbert @corey-oas @Julia.Lake
/content/handbook/engineering/security/infrastructure_security/ @joe-dub @mlancini @pmartinsgl
/content/handbook/engineering/security/ @Julia.Lake @rschulman
/content/handbook/engineering/ @akramer
/content/job-families/engineering/ @joergheilig
/content/job-families/engineering/incubation/ @bmarnane

[finance]
/content/handbook/finance/documents/ @james.shen @watson.lin
/content/handbook/finance/ @brobins
/content/job-families/finance/ @brobins

[legal]
/content/handbook/legal-and-compliance/ @rschulman
/content/job-families/legal-and-corporate-affairs/ @robin

[marketing]
/content/handbook/marketing/marketing-strategy-and-platform/ @RLeihe268
/content/handbook/marketing/marketing-strategy-and-platform/growth/ @s_awezec
/content/handbook/marketing/marketing-strategy-and-platform/marketing-operations/ @christinelee
/content/handbook/marketing/ @akramer
/content/job-families/marketing/ @akramer

[people]
/content/job-families/people-group/recruiting-operations-insights/ @sytses @streas @wendybarnes @rallen3
/content/job-families/people-group/recruiter.md @sytses @streas @wendybarnes @rallen3
/content/job-families/people-group/recruiting-sourcer.md @sytses @streas @wendybarnes @rallen3
/content/job-families/people-group/candidate-experience.md @sytses @streas @wendybarnes @rallen3
/content/job-families/people-group/ @wendybarnes @pegan

[product]
/content/handbook/direction/ @david @fseifoddini @brhea
/content/handbook/product/ @david @fseifoddini @brhea
/content/handbook/product/efficiency/ @joshlambert
/content/handbook/product/saas-efficiency/ @ipedowitz @dsteer @s_awezec @joshlambert @gl-free-saas-user-efficiency-leaders
/data/performance_indicators/ @david @fseifoddini @brhea
/content/handbook/company/performance-indicators/product/ @david @fseifoddini @brhea
/data/performance_indicators/enablement_section.yml @joshlambert
/content/handbook/company/performance-indicators/product/ops-section/ @kbychu @jreporter
/data/performance_indicators/ops_section.yml @kbychu @jreporter
/data/performance_indicators/sec_section.yml @hbenson
/data/performance_indicators/dev_section.yml @ogolowinski
/data/performance_indicators/data_science_section.yml @hbenson @tmccaslin
/content/handbook/product/fulfillment/ @ofernandez2 @tgolubeva @courtmeddaugh @alex_martin @doniquesmit
/content/handbook/product/fulfillment/cloudlicensing/ @courtmeddaugh
/content/handbook/product/fulfillment/provision/ @courtmeddaugh
/content/handbook/handbook/product/fulfillment/licensingissues/ @courtmeddaugh
/content/handbook/product/fulfillment/self-service-purchase/ @alex_martin
/content/handbook/product/fulfillment/qsr/ @tgolubeva
/content/handbook/product/fulfillment/auto-renewal/ @tgolubeva
/content/handbook/company/performance-indicators/product/fulfillment-section/ @ofernandez2 @tgolubeva @courtmeddaugh @alex_martin @doniquesmit
/content/job-families/product/technical-writer.md @susantacker @dianalogan @kpaizee
/content/job-families/product/product-design-management.md @vkarnes
/content/job-families/product/product-designer.md @vkarnes
/content/job-families/product/ux-fullstack-engineer.md @susantacker
/content/job-families/product/ux-research-manager.md @asmolinski2
/content/job-families/product/ux-research-operations-coordinator.md @asmolinski2
/content/job-families/product/ux-researcher.md @asmolinski2
/content/job-families/product/technical-writing-manager.md @susantacker @dianalogan @kpaizee
/content/job-families/product/pricing.md @justinfarris
/content/job-families/product/product-analyst.md @justinfarris @cbraza 
/content/job-families/product/product-management-leadership.md @david @hbenson @mflouton @justinfarris 
/content/job-families/product/product-manager.md @david @justinfarris @hbenson @mflouton @joshlambert @fzimmer @jreporter @ofernandez2 
/content/job-families/product/product-operations.md @justinfarris 
/content/job-families/product/ @david @clenneville @mflouton @justinfarris @hbenson


[sales]
/content/handbook/sales/ @dhong
/content/handbook/company/performance-indicators/sales.md @dhong
/data/performance_indicators/sales.yml @dhong
/content/job-families/sales/ @dhong @jbrennan1 @jakebielecki @james_harrison @jdbeaumont @cfarris

[Security]
/content/handbook/security/ @robin @jlongo_gitlab
/content/job-families/security/ @robin @jlongo_gitlab

[support]
/content/handbook/support/ @lyle @tcooney

[support-readiness]
/content/handbook/support/readiness @lyle @jcolyer

[TeamOps]
/content/teamops/ @streas @lfarrer @cynthia
/content/handbook/teamops/ @streas @lfarrer @cynthia

[total-rewards]
/content/handbook/total-rewards/ @brittanyr @mwilkins

[Values]
/content/handbook/values/ @sytses @streas @lfarrer @ipedowitz
